package Config

import java.sql.Timestamp

case class Client(
                   ID: Long,
                   Nom: String,
                   Prenom: String,
                   Adresse: String,
                   DateDeSouscription: Timestamp)

