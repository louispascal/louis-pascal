package delete

import Config.Client
import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.LongType

object delete {


  def delete(dataset: Dataset[Client], id: Long): Dataset[Client] = {
    val dt = dataset.filter(!col("ID").isin(id))
    dt

  }

  def deleteClient(sparkSession: SparkSession, id: Long): Unit = {
    val dataset = readDS(sparkSession, "data")

    val dt_clean = delete(dataset, id)

    writeDS("temp", dt_clean)
  }


  def readDS(sparkSession: SparkSession, path: String): Dataset[Client] = {

    import sparkSession.implicits._

    val ds: Dataset[Client] = sparkSession
      .read
      .option("header", true)
      .option("delimiter", ",")
      .csv(path).coalesce(1)
      .withColumn("ID", 'ID.cast(LongType))
      .as[Client]

    ds
  }

  def writeDS(path: String, ds: Dataset[Client]): Unit = {
    ds.write
      .option("header", true)
      .option("delimiter", ";")
      .mode("overwrite")
      .csv(path)
  }
}
