import Config.Configuration
import hash.hashing
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType, TimestampType}
import org.apache.spark.sql.{DataFrame, SparkSession}
import scopt.OParser
import delete.delete

import java.sql.Timestamp
import scala.sys.exit

object Main {


  val builder = OParser.builder[Configuration]
  val parser1 = {
    import builder._
    OParser.sequence(
      programName("TestLouisPASCAL"),
      head("TestLouisPASCAL", "0.1"),
      opt[Long]('d', "delete")
        .action((x, c) => c.copy(delete = x))
        .text("delete the specified ID")
        .validate(x =>
          if(x >= 1) success
          else failure("The id must be an integer over 0")
        ),
      opt[Long]('h', "hash")
        .optional()
        .action((s, c) => c.copy(hash = s))
        .text("hash by id")
        .validate(s =>
          if (s >= 1) success
          else failure("The id must be an integer over 0")))
  }

  // OParser.parse returns Option[Configuration]
  def main(args: Array[String]): Unit = {
    val deleteInput = 0
    val hashInput = 0
    OParser.parse(parser1, args, Config()) match {
      case Some(config) =>
        val sparkSession = SparkSession.builder()
          .master("local")
          .getOrCreate()
        if (deleteInput > 1) {
          delete.deleteClient(sparkSession, deleteInput)
        }
        if (hashInput>1){
          hashing.hashClient(sparkSession, hashInput)
        }
      //
      case _ => exit(1)
    }
  }
}
