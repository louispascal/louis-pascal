package hash

import org.apache.spark.sql.{Column, DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.functions.{col, hash}
import org.apache.spark.sql.types.LongType

object hashing {


  def hashing(sparkSession: SparkSession, id: Long): DataFrame = {

    val dt = readDS(sparkSession, "data").coalesce(1)
    val dataframe = dt.filter(!col("ID").isin(id))
    val hnom = dataframe.withColumn("hNom",hash(dataframe.col("Nom")))
    val hprenom = dataframe.withColumn("hPrenom",hash(dataframe.col("Prenom")))
    val hadresse = dataframe.withColumn("hAdresse",hash(dataframe.col("Adresse")))

    val tr1 = hnom.withColumnRenamed("id","idn")
    val tr2 = tr1.withColumnRenamed("date","daten")
    val tr3 = dataframe.join(tr2,dataframe("id")=== tr2("idn"),"inner")

    val tr4 = hprenom.withColumnRenamed("id","idp")
    val tr5 = tr4.withColumnRenamed("date","datep")
    val tr6 = tr3.join(tr5,dataframe("id")=== tr5("idn"),"inner")

    val tr7 = hadresse.withColumnRenamed("id", "ida")
    val tr8 = tr7.withColumnRenamed("date", "datea")
    val tr9 = tr6.join(tr8, dataframe("id") === tr8("ida"),"inner")

    val done = tr9.select("id","hNom","hPrenom","hAdresse","date")


    done.show()
    done

  }

  def hashClient(sparkSession: SparkSession, id: Long): Unit = {

    val dt = hashing(sparkSession, id)

    writeDS("temp", dt)
  }

  def readDS(sparkSession: SparkSession, path: String): DataFrame = {

    import sparkSession.implicits._

    val df: DataFrame = sparkSession
      .read
      .option("header", true)
      .option("delimiter", ",")
      .csv(path).coalesce(1)
      .withColumn("ID", 'ID.cast(LongType))

    df
  }

  def writeDS(path: String, ds: DataFrame): Unit = {
    ds.write
      .option("header", true)
      .option("delimiter", ";")
      .mode("overwrite")
      .csv(path)
  }

}

}
